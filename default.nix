let
  nixpkgs = builtins.fetchTarball {
    url =
      "https://github.com/ehmry/nixpkgs/archive/32751f50996939c2ac11229ecd0ce2ec68d05ac6.tar.gz";
    sha256 = "08g8p0byv33scay40cd8p44szrvz0qhwbnxsyvlv19fz0nf7yzwk";
  };

  systems = {
    localSystem = [ "x86_64-linux" ]; # build platforms
    crossSystem = [ "aarch64-genode" "x86_64-genode" ]; # target platforms
  };

  lib = let prev = import "${nixpkgs}/lib";
  in prev.extend (final: prev: {

    systemSpace =
      # All combinations of build and target systems
      prev.cartesianProductOfSets systems;

    forAllLocalSystems =
      # Apply a function over all self-hosting (Linux) systems.
      f:
      prev.genAttrs systems.localSystem (system: f system);

    forAllCrossSystems =
      # Apply a function over all cross-compiled systems (Genode).
      f:
      with builtins;
      let
        f' = { localSystem, crossSystem }:
          let system = localSystem + "-" + crossSystem;
          in {
            name = crossSystem;
            value = f { inherit system localSystem crossSystem; };
          };
        list = map f' final.systemSpace;
        attrSet = listToAttrs list;
      in attrSet;

    forAllSystems =
      # Apply a function over all Linux and Genode systems.
      f:
      (final.forAllCrossSystems f) // (final.forAllLocalSystems (system:
        f {
          inherit system;
          localSystem = system;
          crossSystem = system;
        }));

    nixosSystem =
      # A derivative of the function for generating Linux NixOS systems.
      # This one is not so well tested…
      { modules, ... }@args:
      import "${nixpkgs}/nixos/lib/eval-config.nix" (args // {
        lib = final;

        baseModules =
          # TODO: do not blacklist modules for the Linux guests
          with builtins;
          let
            isNotModule = suffix:
              let x = "${nixpkgs}/nixos/modules/${suffix}";
              in y: x != y;

            filters = map isNotModule
              (import ./nixos-modules/base-modules-blacklist.nix);

            isCompatible = p: let p' = toString p; in all (f: f p') filters;

          in filter isCompatible
          (import "${nixpkgs}/nixos/modules/module-list.nix");

        modules = modules ++ [
          ({ config, lib, ... }: {
            options = with lib; {

              system.boot.loader.id = mkOption {
                internal = true;
                default = "";
              };

              system.boot.loader.kernelFile = mkOption {
                internal = true;
                default = pkgs.stdenv.hostPlatform.platform.kernelTarget;
                type = types.str;
              };

              system.boot.loader.initrdFile = mkOption {
                internal = true;
                default = "initrd";
                type = types.str;
              };

              systemd.defaultUnit = mkOption {
                default = "multi-user.target";
                type = types.str;
              };

            };
            config = {

              boot.loader.grub.enable = lib.mkDefault false;

              fileSystems."/" = { };

              networking.enableIPv6 = lib.mkForce false;
              systemd.network.enable = lib.mkForce false;

              system.build.toplevel = config.system.build.initXml;

            };

          })
        ];
      });

  });

  flakes = {
    nimble = builtins.getFlake
      "github:nix-community/flake-nimble/54dd3e91c0b61ea63eb4065f95b97f3f0fc98b06";
    eris = builtins.getFlake
      "git+https://codeberg.org/eris/nix-eris?ref=refs/heads/trunk&rev=c190cc13e5f2518913a266180a26b54d39dbcc87";
  };

in rec {

  inherit lib;

  overlay = import ./overlay;

  legacyPackages = let
    overlays = [ flakes.nimble.overlay flakes.eris.overlays.default overlay ];
  in lib.forAllSystems ({ system, localSystem, crossSystem }:
    if system == "x86_64-linux" then
      import nixpkgs { inherit overlays; }
    else
      import nixpkgs {
        inherit localSystem;
        crossSystem = {
          system = crossSystem;
          useLLVM = true;
        };
        config.allowUnsupportedSystem = true;
        inherit overlays;
      });

  packages =
    # Genode native packages, not packages in the traditional
    # sense in that these cannot be installed within a profile.
    lib.forAllCrossSystems ({ system, localSystem, crossSystem }:
      lib.filterAttrs (_: v: v != null)
      legacyPackages.${crossSystem}.genodePackages);

  nixosModules =
    # Modules for composing Genode and NixOS.
    import ./nixos-modules { inherit legacyPackages; };

  checks =
    # Checks for continous testing.
    let tests = import ./tests;
    in with (lib.forAllCrossSystems ({ system, localSystem, crossSystem }:
      tests {
        inherit system localSystem crossSystem;
        hostPkgs = legacyPackages.${localSystem};
        testPkgs = legacyPackages.${crossSystem};
        modulesPath = "${nixpkgs}/nixos/modules";
      } // {
        ports = legacyPackages.${localSystem}.symlinkJoin {
          name = "ports";
          paths = (builtins.attrValues packages.${system}.genodeSources.ports);
        };
      })); {
        x86_64-linux = aarch64-genode // x86_64-genode;
      };

  hydraJobs = lib.attrsets.mapAttrs lib.hydraJob checks;

}
