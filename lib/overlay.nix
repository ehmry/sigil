final: prev:
let lib = prev;
in {

  getMainProgram = pkg:
    with builtins;
    if hasAttr "mainProgram" pkg.meta then
      pkg.meta.mainProgram
    else
      trace "${pkg.name} is missing meta.mainProgram" pkg.pname;

  getEris = filename: pkg:
    with builtins;
    let
      manifest = fromJSON (unsafeDiscardStringContext
        (readFile "${pkg}/nix-support/eris-manifest.json"));
      entry = manifest.${filename};
    in {
      inherit (entry) cap;
      closure = map (ce:
        if ce.cap == entry.cap then
          ce // {
            # hack to build a string with context to propagate a
            # dependency on the rest of the closure
            path = "${pkg}${
                substring (stringLength pkg) (stringLength ce.path) ce.path
              }";
          }
        else
          ce) entry.closure;
    };

  getErisMainProgram = pkg:
    final.getEris (final.getMainProgram pkg) (prev.getOutput "bin" pkg);

  getErisLib = filename: pkg: final.getEris filename (prev.getOutput "lib" pkg);

  uuidFrom = seed:
    let digest = builtins.hashString "sha256" seed;
    in (lib.lists.foldl ({ str, off }:
      n:
      let chunk = builtins.substring off n digest;
      in {
        str = if off == 0 then chunk else "${str}-${chunk}";
        off = off + n;
      }) {
        str = "";
        off = 0;
      } [ 8 4 4 4 12 ]).str;
}
