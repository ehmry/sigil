final: prev:
with prev;

let
  # Helper functions to override package dependent
  # on whether the host or target system is Genode.

  overrideHost = attrs: drv:
    if stdenv.hostPlatform.isGenode then drv.override attrs else drv;

  overrideAttrsHost = f: drv:
    if stdenv.hostPlatform.isGenode then drv.overrideAttrs f else drv;

  overrideAttrsTarget = f: drv:
    if stdenv.targetPlatform.isGenode then drv.overrideAttrs f else drv;

  addPatches' = patches: attrs: { patches = attrs.patches or [ ] ++ patches; };

  addPatchesHost = ps: overrideAttrsHost (addPatches' ps);

  addPatchesTarget = ps: overrideAttrsTarget (addPatches' ps);

  autoreconfHost =
    overrideAttrsHost (_: { nativeBuildInputs = [ final.autoreconfHook ]; });

  nullPkgs =
    # Nullify these packages to find problems early.
    if stdenv.hostPlatform.isGenode then
      builtins.listToAttrs (map (name: {
        inherit name;
        value = final.hello;
      }) [ "iproute2" "strace" ])
    else
      { };

in nullPkgs // {

  bash = overrideAttrsTarget (attrs: {
    configureFlags = attrs.configureFlags
      ++ [ "--without-bash-malloc" ]; # no sbrk please
    postPatch = "sed '/check_dev_tty/d' shell.c";
  }) prev.bash;

  binutils-unwrapped = overrideAttrsTarget (attrs: {
    patches = attrs.patches ++ [
      ./binutils/support-genode.patch
      # Upstreamed, remove at next release.
    ];
    nativeBuildInputs = attrs.nativeBuildInputs
      ++ [ final.updateAutotoolsGnuConfigScriptsHook ];
  }) prev.binutils-unwrapped;

  cmake =
    # TODO: upstream
    overrideAttrsTarget (attrs: {
      postInstall = with stdenv; ''
        local MODULE="$out/share/cmake-${
          lib.versions.majorMinor attrs.version
        }/Modules/Platform/Genode.cmake"
        if [ -e "$MODULE" ]; then
            echo "Upstream provides $MODULE!"
            exit 1
        fi
        cp ${./cmake/Genode.cmake} $MODULE
      '';
    }) prev.cmake;

  coreutils = overrideHost {
    gmp = null;
    libiconv = null;
  } (overrideAttrsHost (_: {
    configureFlags = [
      "--disable-acl"
      "--disable-largefile"
      "--disable-xattr"
      "--disable-libcap"
      "--disable-nls"
    ];
    LDFLAGS = [ "-Wl,--no-as-needed" ];
    # keep libposix NEEDED
  }) prev.coreutils);

  erisPatchHook =
    final.buildPackages.nimPackages.callPackage ./eris-patch-hook {
      patchelf = prev.patchelf.overrideAttrs (attrs: {
        patches = attrs.patches or [ ] ++ [
          ./patchelf/dynstr.patch
          ./patchelf/shiftFile.patch
          ./patchelf/disable-assert.patch
          ./patchelf/rpath.patch
        ];
      });
    };

  genodeLibcCross = callPackage ./libc { };

  genodePackages =
    # The Genode-only packages.
    import ../packages { inherit final prev; };

  getdns = overrideHost { doxygen = null; } prev.getdns;

  grub2 =
    # No need for a Genode build of GRUB.
    if stdenv.targetPlatform.isGenode then
      prev.buildPackages.grub2
    else
      prev.grub2;

  lib = prev.lib.extend (import ../lib/overlay.nix);

  libcCrossChooser = name:
    if stdenv.targetPlatform.isGenode then
      final.genodeLibcCross
    else
      prev.libcCrossChooser name;

  libsodium = overrideAttrsHost (_: {
    patches = (attrs.patches or [ ]) ++ [
      ./libsodium/genode.patch
      # https://github.com/jedisct1/libsodium/pull/1006
    ];
  }) prev.libsodium;

  libkrb5 =
    # Do not want.
    autoreconfHost prev.libkrb5;

  libtool =
    # Autotools related nonesense. Better to compile
    # everything static than to deal with this one.
    overrideAttrsTarget ({ nativeBuildInputs, ... }: {
      nativeBuildInputs = with final;
        nativeBuildInputs ++ [ autoconf automake115x ];
      patches = ./libtool/genode.patch;
    }) prev.libtool;

  libtoxcore = overrideHost {
    libopus = null;
    libvpx = null;
  } prev.libtoxcore;

  linuxPackages =
    # Dummy package.
    if stdenv.hostPlatform.isGenode then {
      extend = _: final.linuxPackages;
      features = { };
      kernel = {
        version = "999";
        config = {
          isEnabled = _: false;
          isYes = _: false;
        };
      };
    } else
      prev.linuxPackages;

  llvmPackages = if stdenv.targetPlatform.isGenode then
    final.llvmPackages_11
  else
    prev.llvmPackages;

  llvmPackages_11 = (import ./llvm-11/override.nix { inherit final prev; });

  ncurses =
    # https://invisible-island.net/autoconf/
    # Stay clear of upstream on this one.
    addPatchesHost [ ./ncurses/genode.patch ] prev.ncurses;

  nimNoLibs = nim.override { stdenv = stdenvNoLibs; };

  nimPackages =
    # Packages from the Nimble flake with adjustments.
    prev.nimPackages.overrideScope' (final': prev':
      with final'; {
        buildNimPackage = if stdenv.hostPlatform.isGenode then
          ({ nimFlags ? [ ], ... }@args:
            prev'.buildNimPackage (args // {
              nimBackend = "cpp";
              nimDefines.posix = { };
              doCheck = false;
            }))
        else
          prev'.buildNimPackage;

        cbor = buildNimPackage rec {
          pname = "cbor";
          version = "20230619";
          src = fetchFromSourcehut {
            owner = "~ehmry";
            repo = "nim_${pname}";
            rev = version;
            hash = "sha256-F6T/5bUwrJyhRarTWO9cjbf7UfEOXPNWu6mfVKNZsQA=";
          };
        };

        dhall = buildNimPackage rec {
          pname = "dhall";
          version = "20230928";
          src = prev.fetchFromSourcehut {
            owner = "~ehmry";
            repo = "${pname}-nim";
            rev = version;
            hash = "sha256-IoSA6yVFiAXuoApjWiz4DohnenlQdTAhfE3VeMSZKtM=";
          };
          propagatedBuildInputs = [ cbor ];
        };

        genode = prev'.genode.overrideAttrs ({ ... }: rec {
          version = "20221020";
          src = final.fetchFromSourcehut {
            owner = "~ehmry";
            repo = "nim_genode";
            rev = version;
            hash = "sha256-kUbhYKoN/H/fP7ekDYY5rZwZY8UDdcbFBEodthFkmYk=";
          };
        });

        preserves = overrideAttrsTarget ({ ... }: rec {
          pname = "preserves";
          version = "20221020";
          src = final.fetchFromGitea {
            domain = "git.syndicate-lang.org";
            owner = "ehmry";
            repo = "${pname}-nim";
            rev = version;
            sha256 = "sha256-yUyt4hAKvEKTFV9KF8994k7RrirQQJ2Jv7ngjbnfXgw=";
          };
          dontFixup = true; # this is breaking input propagation?
        }) prev'.preserves;

        syndicate = overrideAttrsTarget ({ ... }: {
          dontFixup = true; # this is breaking input propagation?
        }) prev'.syndicate;

      });

  openssl =
    overrideHost { static = true; } # shared library comes out stupid big
    (overrideAttrsHost (attrs: {
      outputs = [ "out" ]
        ++ builtins.filter (x: x != "bin" && x != "out") attrs.outputs;
      patches = attrs.patches or [ ] ++ [ ./openssl/genode.patch ];
      configureScript = {
        x86_64-genode = "./Configure genode-x86_64";
        aarch64-genode = "./Configure genode-aarch64";
      }.${stdenv.hostPlatform.system} or (throw
        "Not sure what configuration to use for ${stdenv.hostPlatform.config}");
      configureFlags = attrs.configureFlags ++ [ "no-devcryptoeng" ];
      postInstall =
        "rm $out/bin/c_rehash"; # eliminate the perl runtime dependency
    }) prev.openssl);

  patchelf = addPatchesTarget [
    ./patchelf/dynstr.patch
    # Patch to fix a bug in rewriting the .dynstr section.
  ] prev.patchelf;

  readelferislinks =
    final.nimPackages.callPackage ../packages/readelferislinks { };

  rsync = overrideHost {
    enableACLs = false;
    popt = null;
  } (overrideAttrsHost (_: { outputs = [ "out" "man" ]; }) rsync);

  sculptUtils = callPackage ./sculpt-utils { };

  solo5-tools = callPackage ./solo5-tools { };

  stdenv = if prev.stdenv.hostPlatform.isGenode then
    stdenv.override (prev': {
      extraNativeBuildInputs = prev'.extraNativeBuildInputs
        ++ [ final.buildPackages.erisPatchHook ];
    })
  else
    prev.stdenv;

  tor = overrideAttrsHost (attrs: {
    patches = attrs.patches or [ ] ++ [
      ./tor/genode.patch
      # We don't do users and groups here.
    ];
    postPatch = null; # Avoid torsocks patching
  }) prev.tor;

  zlib = overrideAttrsHost (attrs: {
    postInstall = attrs.postInstall or "" + ''
      pushd ''${!outputLib}/lib
      find . -type l -delete
      mv libz.so.* libz.so
      popd
    '';
  }) prev.zlib;

  zstd = let
    static = true;
    legacySupport = false;
  in overrideAttrsHost (_: rec {
    cmakeFlags = lib.attrsets.mapAttrsToList
      (name: value: "-DZSTD_${name}:BOOL=${if value then "ON" else "OFF"}") {
        BUILD_SHARED = !static;
        BUILD_STATIC = static;
        PROGRAMS_LINK_SHARED = !static;
        LEGACY_SUPPORT = legacySupport;
        BUILD_TESTS = doCheck;
      };
    doCheck = stdenv.hostPlatform == stdenv.buildPlatform;
  }) prev.zstd;

}
