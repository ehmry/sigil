{ lib, buildNimPackage, bintools-unwrapped, patchelf, base32, bigints, cbor, dhall, eris }:

buildNimPackage {
  name = "eris_patch";
  buildInputs = [ base32 bigints cbor dhall eris ];
  patchelf = "${lib.getBin patchelf}/bin/patchelf";
  objcopy = "${lib.getBin bintools-unwrapped}/bin/objcopy";
  unpackPhase = "install -D ${./eris_patch.nim} ./src/eris_patch.nim";
  nimBinOnly = true;
  preConfigure = ''
    cat << EOF > eris_patch.nimble
    bin = @[ "src/eris_patch"]
    EOF
  '';
  setupHook = ./eris_patch.sh;
}
