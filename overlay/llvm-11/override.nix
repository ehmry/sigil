{ final, prev }:

let
  addPatches = patches': drv:
    (drv.overrideAttrs
      ({ patches ? [ ], ... }: { patches = patches ++ patches'; }));

  libraries = prev.llvmPackages_11.libraries.extend (final': prev': {

    libcxxabi = prev'.libcxxabi.overrideAttrs ({ cmakeFlags, ... }: {
      cmakeFlags = cmakeFlags ++ [ "-DLIBCXXABI_ENABLE_THREADS=OFF" ];
    });

    libcxx = prev'.libcxx.overrideAttrs ({ cmakeFlags, patches ? [ ], ... }: {
      patches = patches ++ [ ./libcxx-genode.patch ];
    });

  });

  tools = prev.llvmPackages_11.tools.extend (final': prev': {

    libllvm = addPatches [ ./llvm-genode.patch ] prev'.libllvm;

    lld = addPatches [ ./lld-genode.patch ] prev'.lld;

    libclang = prev'.libclang.overrideAttrs
      ({ patches ? [ ], postPatch, ... }: {
        patches = patches ++ [ ./clang-genode.patch ];
        postPatch = postPatch + ''
          sed -i -e 's/lgcc_s/lgcc_eh/' lib/Driver/ToolChains/*.cpp
        '';
      });

    clangUseLLVM = let inherit (final.genodePackages) genodeSources;
    in (prev'.clangNoLibcxx.override {
      gccForLibs = genodeSources.toolchain.cc;
      nixSupport = {
        cc-cflags = [
          "--gcc-toolchain=${genodeSources.toolchain.cc}"
          "--sysroot=${genodeSources.genodeBase}"
          "-I${genodeSources.genodeBase}/include"
          "-L${genodeSources.genodeBase}"
        ];
        libcxx-ldflags = [ "${final.genodePackages.stdcxx}/lib/stdcxx.lib.so" ];
      };
    }).overrideAttrs ({ postFixup, ... }: {
      postFixup = postFixup + (with final; ''

        for dir in ${genodeSources.toolchain.cc}/${stdenv.targetPlatform.config}/include/c++/*; do
          echo "-isystem $dir" >> $out/nix-support/libcxx-cxxflags
        done
        echo "-isystem ${genodeSources}/repos/libports/include/stdcxx" >> $out/nix-support/libcxx-cxxflags
        for dir in ${genodeSources.ports.stdcxx}/*/include/stdcxx; do
          echo "-isystem $dir" >> $out/nix-support/libcxx-cxxflags
          echo "-isystem $dir/std" >> $out/nix-support/libcxx-cxxflags
          echo "-isystem $dir/c_global" >> $out/nix-support/libcxx-cxxflags
        done
        echo "${genodePackages.stdcxx}/lib/stdcxx.lib.so" >> $out/nix-support/libcxx-ldflags
      '');
    });

    clangNoLibcxx = prev'.clangNoLibcxx.override (with final.genodePackages; {
      nixSupport = {
        cc-cflags = [
          "--gcc-toolchain=${genodeSources.toolchain.cc}"
          "--sysroot=${genodeSources.genodeBase}"
          "-I${genodeSources.genodeBase}/include"
        ];
        cc-ldflags = [ "-L${genodeSources.genodeBase}" ];
      };
    });

  });

in { inherit libraries tools; } // libraries // tools # awkward
