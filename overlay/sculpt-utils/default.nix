{ lib, hostPlatform, dhall, libxml2, runCommand, xz }:

let arch = hostPlatform.uname.processor;
in rec {

  storeVersion = builtins.substring 11 7;

  buildBinArchive = inputPkg:
    runCommand "bin-${arch}-${inputPkg.pname}-${inputPkg.version}" (rec {
      inherit arch inputPkg;
      inherit (inputPkg) pname;
      version = builtins.substring 11 7 inputPkg;
      nativeBuildInputs = [ dhall xz ];
      depotPath = "bin/${arch}/${pname}/${version}";
      srcPath = "src/${pname}/${version}";
    }) ''
      mkdir -p archive/$version $out/bin/$arch/$pname
      pushd archive/$version
      export XDG_CACHE_HOME=$NIX_BUILD_TOP
      eval $(echo "${
        ./generate-manifest-link-script.dhall
      } (toMap $inputPkg/nix-support/eris-manifest.dhall)" | dhall text)
      # Need copies of truncated URNs because various Genode subsystems do this
      for urn in urn:eris:*
      do
        cp ''${urn} ''${urn:0:63}
        cp ''${urn} ''${urn:0:59}
      done
      popd
      pushd archive
      tar -c --xz -f $out/$depotPath.tar.xz --dereference .
      popd
    '';

  buildIndex = { release, supportsArch, indexes }:
    let
      supportsArch' =
        lib.strings.concatMapStrings (arch: ''<supports arch="${arch}"/>'')
        supportsArch;

      pkgToString = { info, path }: ''<pkg info="${info}" path="${path}"/>'';
      indexToString = name:
        { pkgs }:
        ''<index name="${name}">${toString (map pkgToString pkgs)}</index>'';

      indexes' = toString (lib.attrsets.mapAttrsToList indexToString indexes);

    in runCommand "index-${release}" { nativeBuildInputs = [ libxml2 ]; } ''
      mkdir -p $out/index
      xmllint -format - << EOF | xz > $out/index/${release}.xz
      <index>
      ${supportsArch'}
      ${indexes'}
      </index>

      EOF
    '';

  tagLabelToString = { tag, label ? null }:
    "<${tag} ${lib.optionalString (label != null) ''label="${label}"''}/>";

  runtimeToString = { binary, caps, ram, requires ? [ ], content ? [ ]
    , config ? "<config/>", provides ? [ ] }:
    ''
      <runtime binary="${binary}" caps="${toString caps}" ram="${toString ram}">
    ''

    + (lib.optionalString (requires != [ ]) ''
      <requires>${toString (map tagLabelToString requires)}</requires>
    '')

    + (lib.optionalString (provides != [ ]) ''
      <provides>${toString (map (p: "<${p}/>") provides)}</provides>
    '')

    + (lib.optionalString (content != [ ]) ''
      <content>${toString (map tagLabelToString content)}</content>
    '')

    + ''
      ${config}
      </runtime>
    '';

  buildPkg = { pname, archives, runtime }:
    let
      drv = runCommand "pkg-${pname}" {
        inherit pname;
        nativeBuildInputs = [ libxml2 xz ];
      } ''
        export version=''${out:11:7}
        mkdir -p $version
        awk 'NF' << EOF > $version/archives
        ${lib.strings.concatMapStrings (s: s + "\n") archives}
        EOF

        xmllint -format - << EOF > $version/runtime
        ${runtimeToString runtime}

        EOF

        mkdir -p $out/pkg/$pname
        tar -c --xz -f $out/pkg/$pname/$version.tar.xz $version
      '';
    in drv // (rec {
      version = storeVersion drv;
      depotPath = "pkg/${drv.pname}/${version}";
    });

}
