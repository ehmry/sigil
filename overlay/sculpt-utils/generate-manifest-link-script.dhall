let ClosureEntry = { cap : Text, path : Text }

let Entry =
      { mapKey : Text, mapValue : { cap : Text, closure : List ClosureEntry } }

in  λ(manifest : List Entry) →
      List/fold
        Entry
        manifest
        Text
        ( λ(entry : Entry) →
          λ(script : Text) →
            ''
              ${List/fold
                  ClosureEntry
                  entry.mapValue.closure
                  Text
                  ( λ(entry : ClosureEntry) →
                    λ(script : Text) →
                      "cp -u ${entry.path} ${entry.cap}; ${script}"
                  )
                  "true;"} cp ${entry.mapValue.cap} ${entry.mapKey}; ${script}
            ''
        )
        "true"
