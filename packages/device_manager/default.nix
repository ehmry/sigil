{ lib, buildNimPackage, fetchFromSourcehut, genode }:

buildNimPackage rec {
  pname = "device_manager";
  version = "0.0";
  outputs = [ "out" "dhall" ];

  src = fetchFromSourcehut {
    owner = "~ehmry";
    repo = pname;
    rev = "4ff7d47b83255a437d862d16b8424a3c05e3eab1";
    hash = "sha256-uCEEm0sZap9PkVwCgKqTzg8CL4vPsHN49ooMPpKhrC4=";
  };

  preConfigure = ''
    echo 'backend = "cpp"' >> ${pname}.nimble
  '';

  nimDefines = [ "posix" ];

  buildInputs = [ genode ];

  postInstall = ''
    install -Dt $dhall config/package.dhall
  '';
}
