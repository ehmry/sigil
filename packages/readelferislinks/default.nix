{ lib, buildNimPackage, cbor, eris }:

buildNimPackage rec {
  pname = "readelferislinks";
  version = "20221012";
  nimBinOnly = true;
  src = ./src;
  preConfigure = ''
    cat << EOF > ${pname}.nimble
    bin = @["${pname}"]
    EOF
  '';
  buildInputs = [ cbor eris ];
  meta = {
    description = "Utility for parsing ERIS links within an ELF binary";
  };
}
