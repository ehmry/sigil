# SPDX-FileCopyrightText: ☭ 2022 Emery Hemingway
# SPDX-License-Identifier: GPL-3.0-or-later.txt

import std/[os, streams, tables]
import cbor, eris

proc atChar(strm: Stream; c: char): bool =
  if strm.atEnd:
    quit "reached end of stream without finding ELF note"
  c == strm.readChar()

proc scanUntil(strm: Stream; pat: string) =
  while true:
    block patLoop:
      for c in pat:
        if not strm.atChar(c): break patLoop
      return

proc parseStream(strm: Stream) =
  # TODO: write an ELF parser.
  strm.scanUntil("Sigil\x00\x00\x00\xD9\xD9\xF7")
    # scan for the ELF tag owner and the self-describing CBOR tag
  let node = strm.readCbor()
  if node.kind == cborUnsigned and node.uint == 0: discard
  elif node.kind != cborMap:
    quit("unrecognized CBOR data: " & $node)
  else:
    for key, val in node.map:
      if key.kind != cborText or val.kind != cborBytes or not val.hasTag(erisCborTag):
        quit("Unrecognized note data " & $node)
      let cap = parseCap(val.bytes)
      stdout.writeLine cap, " ", key.text

proc main =
  var args = commandLineParams()
  if args.len == 0:
    args.add "-"
  for arg in args:
    var str =
      if arg == "-":
        newFileStream(stdin)
      else:
        openFileStream(arg)
    parseStream(str)
    close(str)

main()
