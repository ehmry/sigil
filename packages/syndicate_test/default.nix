{ buildNimPackage, genode, npeg, preserves, syndicate }:

buildNimPackage {
  pname = "syndicate_test";
  version = "unstable";
  nimDefines.posix = { };
  src = ./src;
  propagatedBuildInputs = [ genode npeg preserves syndicate ];
  nimBinOnly = true;
  meta.mainProgram = "syndicate_test";
}
