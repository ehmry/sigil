# SPDX-FileCopyrightText: ☭ 2022 Emery Hemingway
# SPDX-License-Identifier: AGPL-3.0-or-later.txt

import std/[asyncdispatch, options, streams, strutils, xmltree]
import genode, genode/[reports, roms]
import preserves, preserves/xmlhooks, syndicate

type RomAssertion {.preservesRecord: "ROM".} = object
  name: string
  content: XmlNode

type RomCallback = proc (t: var Turn; rom: RomClient) {.closure, gcsafe.}
proc newRomHandler(env: GenodeEnv; label: string; turn: var Turn; cb: RomCallback): RomHandler =
  ## Create a `RomHandler` that dispatches a callback within a Syndicate `Turn`.
  let facet = turn.facet
  result = newRomHandler(env, label) do (rom: RomClient):
    run(facet) do (turn: var Turn):
      cb(turn, rom)

componentConstructHook = proc(env: GenodeEnv) =

  bootDataspace("main") do (ds: Ref; turn: var Turn):
    echo "bootDataspace…"
    # Syndicate boot hook

    onPublish(turn, ds, grab()) do (a: Assertion):
      echo "seen in Syndicate dataspace ", ds, " - ", a

    var configHandle, stateHandle: syndicate.Handle
    let configRom =  newRomHandler(env, "config", turn) do (turn: var Turn; rom: RomClient):
      # config ROM hook
      var ass = RomAssertion(name: "config", content: rom.xml)
      echo "publish ", ass.name, " XML in dataspace: ", ass.content
      replace(turn, ds, configHandle, ass)
