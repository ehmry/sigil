{
  name = "nim";
  machine = { pkgs, ... }: {
    genode.init.children.test_nim = {
      package = with pkgs;
        nimPackages.buildNimPackage {
          pname = "test_nim";
          inherit (nim) version;
          nimDefines.posix = {};
          unpackPhase = ''
            mkdir test
            cd test
            echo 'echo "Hello Nim world!"' > test_nim.nim
            cat << EOF > eris_patch.nimble
            backend = "cpp"
            bin = @[ "test_nim"]
            EOF
          '';
        };
      configFile = builtins.toFile "nim.dhall" ''
        let Sigil = env:DHALL_SIGIL

        let Init = Sigil.Init

        let Child = Init.Child

        in  λ(binary : Text) →
              Child.flat
                Child.Attributes::{
                , binary
                , config = Init.Config::{ attributes = toMap { ld_verbose = "true" } }
                , exitPropagate = True
                , resources = Sigil.Init.Resources::{
                  , caps = 500
                  , ram = Sigil.units.MiB 10
                  }
                }
      '';
    };
  };

  testScript = ''
    start_all()
    machine.wait_until_serial_output("Hello Nim world!")
  '';
}
