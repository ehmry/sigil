{
  name = "syndicate";
  machine = { pkgs, ... }: {
    genode.init.verbose = true;
    genode.init.children.syndicate_service = {
      package = pkgs.genodePackages.syndicate_test;
      extraInputs = with pkgs.genodePackages; [ libc stdcxx ];
      configFile = builtins.toFile "nim.dhall" ''
        let Sigil = env:DHALL_SIGIL

        let Init = Sigil.Init

        let Child = Init.Child

        let Libc = Sigil.Libc

        in  λ(binary : Text) →
              Child.flat
                Child.Attributes::{
                , binary
                , exitPropagate = True
                , resources = Sigil.Init.Resources::{
                  , caps = 500
                  , ram = Sigil.units.MiB 10
                  }
                , routes =
                  [ Sigil.Init.ServiceRoute.parentLabel
                      "ROM"
                      (Some "state")
                      (Some "state")
                  ]
                , config =
                    (Libc.toConfig Libc.default)
                  with attributes = toMap { ld_verbose = "true" }
                }
      '';
    };
  };

  testScript = ''
    start_all()
    machine.wait_until_serial_output("<ROM \"config\"")
  '';
}
